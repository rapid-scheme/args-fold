;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid args-fold-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid runtime-environment)
	  (rapid args-fold))
  (begin
    (define (option-proc option name arg seed)
      (cons (list name arg) seed))

    (define options
      (list (option '(#\h "help") #f #f option-proc)
	    (option '(#\v "version") #f #f option-proc)
	    (option '(#\o "output") #t #f option-proc)))

    (define (unrecognized-option-proc option name arg seed)
      (cons (list 'unrecognized-option name arg) seed))

    (define (operand-proc operand seed)
      (cons (list 'operand operand) seed))

    (define (scan-args args)
      (reverse
       (args-fold args options unrecognized-option-proc operand-proc '())))

    (define (run-tests)
      (test-begin "A program argument processor")

      (test-equal "Long-option argument as separate argument"
		  '(("output" "test.o"))
		  (scan-args '("--output" "test.o")))

      (test-equal "Long-option argument in a single argument string"
		  '(("output" "test.o"))
		  (scan-args '("--output=test.o")))
      
      (test-equal "Parsing options with and without required arguments"
		  '(("help" #f)
		    (#\h #f)
		    (#\v #f)
		    ("output" "file.o")
		    (operand "file.c"))
		  (scan-args '("--help" "-hv" "--output" "file.o" "file.c")))
 
      (test-end))))
